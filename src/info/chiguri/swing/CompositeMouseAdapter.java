package info.chiguri.swing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class CompositeMouseAdapter extends MouseAdapter {
	Set<MouseAdapter> adapters = Collections.synchronizedSet(new HashSet<MouseAdapter>());


	public void addListener(MouseAdapter listener) {
		adapters.add(listener);
	}

	public void removeListener(MouseAdapter listener) {
		adapters.remove(listener); // this may not be used...
	}

	@Override
	public void mousePressed(MouseEvent e) {
		for(MouseAdapter listener : adapters) {
			listener.mousePressed(e);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		for(MouseAdapter listener : adapters) {
			listener.mouseClicked(e);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		for(MouseAdapter listener : adapters) {
			listener.mouseReleased(e);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		for(MouseAdapter listener : adapters) {
			listener.mouseEntered(e);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		for(MouseAdapter listener : adapters) {
			listener.mouseExited(e);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		for(MouseAdapter listener : adapters) {
			listener.mouseMoved(e);
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		for(MouseAdapter listener : adapters) {
			listener.mouseDragged(e);
		}
	}

}
