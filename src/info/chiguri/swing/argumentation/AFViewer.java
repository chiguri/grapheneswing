package info.chiguri.swing.argumentation;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import info.chiguri.graphene.argumentation.Argument;
import info.chiguri.graphene.argumentation.ArgumentationFramework;
import info.chiguri.graphene.argumentation.Attack;
import info.chiguri.graphene.argumentation.DisplayConfig;
import info.chiguri.graphene.argumentation.Label;
import info.chiguri.graphene.argumentation.UnsatisifiablePartialLabelingException;
import info.chiguri.swing.LeftDoubleClickedMouse;
import info.chiguri.swing.canvas.SwingColor;
import info.chiguri.swing.graphene.ClickedMoveMouse;
import info.chiguri.swing.graphene.GPanel;
import info.chiguri.swing.graphene.ModifyGraphMouse;
import info.chiguri.swing.graphene.PNGSaveTrimmingEventHandler;

public class AFViewer {
	static final int LEFT = 20, RIGHT = 680, TOP = 20, BOTTOM = 680;
	static boolean visibleLabelSetFrame = false;

	static ArgumentationFramework testAF(String[] args) {
		if(args.length >= 1) {
			String filename = args[0];
			if(filename.equals("-i")) {
				// どうするかな？ファイル名を受け取る？
			}
			try {
				File file = new File(filename);
				return ArgumentationFramework.generateAF(Files.readAllLines(file.toPath()).toArray(new String[0]));
			} catch (FileNotFoundException e) {
				System.err.println("The file " + filename + " cannot be opened");
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				System.err.println("The file " + filename + " cannot be opened or broken as text");
				e.printStackTrace();
				System.exit(1);
			}
		}
		return new ArgumentationFramework();
	}

	static abstract class MenuAdaptor implements MenuListener {
		@Override
		public void menuDeselected(MenuEvent e) {
			// do nothing
		}

		@Override
		public void menuCanceled(MenuEvent e) {
			// do nothing
		}
	}

	static JMenuBar generateMenuBar(ArgumentationFramework AF) {
		JMenuBar bar = new JMenuBar();

		JMenu labeling = new JMenu("Labeling");
		JMenuItem ground = new JMenuItem("Ground");
		ground.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Map<Argument, Label> map = AF.groundLabeling();
					for(Map.Entry<Argument, Label> entry : map.entrySet()) {
						System.out.println(entry);
						entry.getKey().setLabel(entry.getValue());
					}
				} catch(UnsatisifiablePartialLabelingException ex) {
					ex.printStackTrace();
					//JOptionPane.showMessageDialog(this, new JLabel(ex.toString()));
				}
			}
		});
		labeling.add(ground);

		JMenuItem partial = new JMenuItem("Partial");
		partial.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!visibleLabelSetFrame) {
					JFrame select = new JFrame();
					select.setSize(300, 500); // 多分スクロールバーが必要
					select.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent e) {
							visibleLabelSetFrame = false;
						}
					});
					visibleLabelSetFrame = true;
					select.add(new LabelSetPanel(AF));
					select.setVisible(true);
				}
			}
		});
		labeling.add(partial);

		bar.add(labeling);

		/*
		JMenu select = new JMenu("Select");
		select.addMenuListener(new MenuAdaptor() {
			boolean visible = false;
			@Override
			public void menuSelected(MenuEvent e) {
				if(!visible) {
					JFrame select = new JFrame();
					select.setSize(300, 500);
					select.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent e) {
							visible = false;
						}
					});
					visible = true;
					select.add(new ObjectSelectPanel(exp));
					select.setVisible(true);
				}
			}
		});

		JMenu renum = new JMenu("Renumber");
		renum.addMenuListener(new MenuAdaptor() {
			@Override
			public void menuSelected(MenuEvent e) {
				System.out.println("renumbering...");
				exp.renumber();
				exp.printPLCA(System.out);
			}
		});

		bar.add(select);
		bar.add(renum);
		*/
		return bar;
	}

	public static void initDisplayConfig() {
		DisplayConfig.ArgumentRadius = 10;
		DisplayConfig.ArgumentFontSize = 8;

		DisplayConfig.ArgumentEdgeColor = new SwingColor(Color.black);

		DisplayConfig.InColor = new SwingColor(Color.cyan);
		DisplayConfig.OutColor = new SwingColor(Color.red);
		DisplayConfig.UndecColor = new SwingColor(Color.green);

		DisplayConfig.ValidAttackColor = new SwingColor(Color.black);
		DisplayConfig.InValidAttackColor = new SwingColor(Color.lightGray);
		DisplayConfig.UnclearAttackColor = new SwingColor(Color.gray);

	}

	public static void main(String[] args) {
		initDisplayConfig();
		final ArgumentationFramework AF = testAF(args);
		JFrame m = new JFrame();
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		displayAF(AF, m);
	}

	static int argnum = 0;
	public static void displayAF(ArgumentationFramework AF, final JFrame m) {
		AF.printAF(System.out);

		m.setSize(LEFT+RIGHT, BOTTOM+TOP);
		m.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				AF.printAF(System.out);
			}
		});
		m.setJMenuBar(generateMenuBar(AF));
		GPanel<Argument, Attack> panel = new GPanel<>(AF);
		panel.addMouseAdapter(new LeftDoubleClickedMouse(new PNGSaveTrimmingEventHandler<Argument, Attack>(panel, AF, DisplayConfig.ArgumentRadius+2) {
			@Override
			protected void saveImage(String filename, BufferedImage img) {
				super.saveImage(filename, img);
				try {
					filename += ".txt";
					AF.printAF(new PrintStream(filename));
					System.out.println("Save AF text as " + filename );
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}));
		panel.addMouseAdapter(new ClickedMoveMouse<>(AF));
		panel.addMouseAdapter(new ModifyGraphMouse<>(AF, p -> { return new Argument("A" + argnum++, p); }, (e1, e2) -> { return new Attack(e1, e2); }));
		m.add(panel);
		m.setVisible(true);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				m.repaint();
			}
		}, 100, 100);

	}
}
