package info.chiguri.swing.canvas;

import java.awt.Color;

import info.chiguri.graphene.canvas.AbstractColor;

public class SwingColor extends AbstractColor{
	public final Color c;
	public SwingColor(Color c) {
		this.c = c;
	}
}
