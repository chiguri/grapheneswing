package info.chiguri.swing.canvas;

import java.awt.Font;

import info.chiguri.graphene.canvas.AbstractFont;

public class SwingFont extends AbstractFont {
	public final Font f;
	public SwingFont(Font f) {
		this.f = f;
	}
	@Override
	public AbstractFont deriveFont(float fsize) {
		return new SwingFont(f.deriveFont(fsize));
	}
}
