package info.chiguri.swing.graphene;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import info.chiguri.graphene.graph.Edge;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.graph.NodeInterface;

public class ClickedMoveMouse<G extends NodeInterface, E extends Edge<G>> extends MouseAdapter {
	Graph<G, E> graph;
	public ClickedMoveMouse(Graph<G, E> g) {
		super();
		graph = g;
	}
	G clicked = null;

	@Override
	public void mousePressed(MouseEvent e) {
		clicked = graph.clicked(e.getX(), e.getY());
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(clicked != null) {
			clicked.moveTo(e.getX(), e.getY());
		}
	}
}