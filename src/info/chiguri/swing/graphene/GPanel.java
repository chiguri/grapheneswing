package info.chiguri.swing.graphene;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;

import javax.swing.JPanel;

import info.chiguri.graphene.IterableIterator;
import info.chiguri.graphene.graph.Edge;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.graph.NodeInterface;
import info.chiguri.swing.CompositeMouseAdapter;
import info.chiguri.swing.canvas.SwingCanvas;

public class GPanel<G extends NodeInterface, E extends Edge<G>> extends JPanel {
	final Graph<G, E> graph;
	final CompositeMouseAdapter adapter = new CompositeMouseAdapter();

	public GPanel(Graph<G, E> g) {
		graph = g;
		addMouseListener(adapter);
		addMouseMotionListener(adapter);
	}

	public void addMouseAdapter(MouseAdapter ad) {
		adapter.addListener(ad);
	}

	public void removeMouseAdapter(MouseAdapter ad) {
		adapter.removeListener(ad);
	}

	@Override
	public void paintComponent(Graphics graphics) {
		SwingCanvas g = new SwingCanvas((Graphics2D)graphics);

		g.setAntiAlias(true);

		for(G v : new IterableIterator<>(graph.nodeIterator())) {
			v.draw(g);
		}

		for(E e : new IterableIterator<>(graph.edgeIterator())) {
			e.drawEdge(g, graph.containsEdge(new Edge<G>(e.to, e.from)));
		}
	}
}
