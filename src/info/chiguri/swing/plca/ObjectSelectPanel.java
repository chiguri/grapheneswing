package info.chiguri.swing.plca;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collection;
import java.util.function.Function;

import javax.swing.ButtonGroup;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import info.chiguri.graphene.plca.Expression;
import info.chiguri.graphene.plca.Line;
import info.chiguri.graphene.plca.PLCAobject;
import info.chiguri.graphene.plca.ULine;

public class ObjectSelectPanel extends JPanel {
	public final Expression exp;
	final protected ButtonGroup bg = new ButtonGroup();
	final protected JList<String> list = new JList<>();
	protected PLCAobject[] items;
	final protected JRadioButton buttonP = new JRadioButton("P");
	final protected JRadioButton buttonL = new JRadioButton("L");
	final protected JRadioButton buttonC = new JRadioButton("C");
	final protected JRadioButton buttonA = new JRadioButton("A");
	final protected JRadioButton buttonO = new JRadioButton("o");

	public ObjectSelectPanel(Expression exp) {
		super(true);
		this.exp = exp;

		bg.add(buttonP); bg.add(buttonL); bg.add(buttonC); bg.add(buttonA); bg.add(buttonO);

		buttonP.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					setList(exp.getPoints(), (p) -> exp.toFullString(p));
				}
			}
		});
		buttonL.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					Collection<Line> ls = exp.getLines();
					items = new PLCAobject[ls.size()];
					String[] strs = new String[ls.size()];
					int i = 0;
					for(Line l : ls) {
						items[i] = new ULine(l, exp.getInverse(l));
						strs[i] = exp.toFullString(l);
						++i;
					}

					list.setListData(strs);
				}
			}
		});
		buttonC.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					setList(exp.getCircuits(), (c) -> exp.toFullString(c));
				}
			}
		});
		buttonA.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					setList(exp.getAreas(), (a) -> exp.toFullString(a));
				}
			}
		});
		buttonO.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					exp.clearFocused();
					exp.getO().focused();
				}
			}
		});

		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				exp.clearFocused();
				int [] selected = list.getSelectedIndices();
				for(int i : selected) {
					items[i].focused();
				}
			}
		});

		JPanel radiopanel = new JPanel();
		radiopanel.add(buttonP);
		radiopanel.add(buttonL);
		radiopanel.add(buttonC);
		radiopanel.add(buttonA);
		radiopanel.add(buttonO);

		this.setLayout(new BorderLayout());

		this.add(radiopanel, BorderLayout.NORTH);
		this.add(list, BorderLayout.CENTER);
	}

	protected void clearSelection() {
		list.setListData(new String[0]);
		bg.clearSelection();
	}

	private <T extends PLCAobject> void setList(Collection<T> X, Function<T, String> strer) {
		items = new PLCAobject[X.size()];
		String[] strs = new String[X.size()];
		int i = 0;
		for(T x : X) {
			items[i] = x;
			strs[i] = strer.apply(x);
			++i;
		}

		list.setListData(strs);
	}
}
